package com.magnificitit.training.controllers;

import com.magnificitit.training.dtos.CategoryDTO;
import com.magnificitit.training.entities.Category;
import com.magnificitit.training.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping
    public void save(@RequestBody CategoryDTO categoryDTO) {
        Category category = new Category();
        category.setName(categoryDTO.getName());
        categoryService.save(category);
    }

    @GetMapping
    public List<CategoryDTO> findAll() {
        List<Category> categories = categoryService.findAll();
        List<CategoryDTO> categoryDTOS = new ArrayList<>();

        for (Category c : categories) {
            CategoryDTO categoryDTO = new CategoryDTO();
            categoryDTO.setId(c.getId());
            categoryDTO.setName(c.getName());
            categoryDTOS.add(categoryDTO);
        }
        return categoryDTOS;
    }


}
