package com.magnificitit.training.services;

import com.magnificitit.training.entities.Category;

import java.util.List;

public interface CategoryService {

    void save(Category category);
    List<Category> findAll();

}
